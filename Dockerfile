# File: avc-maven-docker/Dockerfile
#
# Use to build the image: avcompris/maven-docker

FROM maven:3.8.4-openjdk-17-slim
MAINTAINER david.andriana@avantage-compris.com


# 1. STANDARD PACKAGES

RUN apt-get update

RUN apt-get install -y \
	apt-utils \
	curl \
	gnupg2 \
	libxml2-utils \
	software-properties-common \
	sudo

RUN echo "ci ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/ci

# 2. LOCALES

RUN apt-get install -y locales

RUN locale-gen "en_US.UTF-8"

RUN echo "LANG=en_US.UTF-8" >> /etc/default/locale
RUN echo "LC_ALL=en_US.UTF-8" >> /etc/default/locale

# 3. DOCKER

RUN curl -sSL https://get.docker.com/ | sh

# 4. USERS

RUN useradd -m -s /bin/bash ci

# WARNING: The MAVEN_CONFIG will be unset by /usr/local/bin/mvn-entrypoint.sh
#
ENV MAVEN_CONFIG=/var/lib/maven

RUN mkdir -p /var/lib/maven

RUN chown ci:ci /var/lib/maven

USER ci
WORKDIR /home/ci

RUN mkdir -p .m2/repository/


# 5. BUILDINFO

COPY buildinfo /


# 9. END

USER ci
WORKDIR /home/ci
